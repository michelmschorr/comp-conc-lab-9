# Computação Concorrente

## Laboratório 9

<br>

### Aluno:  Michel Monteiro Schorr<br>
### DRE:  120017379




<br><br>




## Estrutura
### Dentro da pasta lab, irá encontrar 3 pastas: 
**src**, contendo o condigo fonte principal <br>
**libs**, contendo as bibliotecas usadas e o arquivo include.h  <br>
**execs**, contendo os executaveis   <br>





<br><br>




## Comandos

Os comandos para compilar e executar são os seguintes:


<br>


``` 
gcc ./lab/src/lab.c -o ./lab/execs/lab -lpthread
./lab/execs/lab
```




<br><br>




## Dois exemplos:
``` 
[michelms@michel-pc1 lab9]$ ./lab/execs/lab
Seja bem-vindo!
Fique a vontade.        
Sente-se por favor.         <- (linha 3)
Aceita um copo d’agua?
Volte sempre!
[michelms@michel-pc1 lab9]$ ./lab/execs/lab
Seja bem-vindo!
Fique a vontade.                
Aceita um copo d’agua?
Sente-se por favor.         <- (linha 4)
Volte sempre!
[michelms@michel-pc1 lab9]$
``` 
