/*
Computação Concorrente - Lab 9
Michel Monteiro Schorr
120017379
*/

//includes
#include "../libs/include.h"




//variaveis globais
sem_t sem_meio, sem_fim;
int threads_centrais_concluidas = 0;
#define NTHREADS  5




//thread 5
void *t5 (void *arg) {
    printf("Seja bem-vindo!\n");

    sem_post(&sem_meio);

    pthread_exit(NULL);
}




//thread 2
void *t2 (void *arg) {
    sem_wait(&sem_meio);   //espera t5 ou outra das threads centrais liberar

    printf("Fique a vontade.\n");
    threads_centrais_concluidas++;

    if(threads_centrais_concluidas>=3) //caso as 3 threads centrais tenham sido concluidas, libera a thread 1 (a final)
        sem_post(&sem_fim);

    sem_post(&sem_meio);


    pthread_exit(NULL);
}




//thread 3
void *t3 (void *arg) {
    sem_wait(&sem_meio);   //espera t5 ou outra das threads centrais liberar

    printf("Sente-se por favor.\n");
    threads_centrais_concluidas++;

    if(threads_centrais_concluidas>=3) //caso as 3 threads centrais tenham sido concluidas, libera a thread 1 (a final)
        sem_post(&sem_fim);

    sem_post(&sem_meio);


    pthread_exit(NULL);
}




//thread 4
void *t4 (void *arg) {
    sem_wait(&sem_meio);   //espera t5 ou outra das threads centrais liberar

    printf("Aceita um copo d’agua?\n");
    threads_centrais_concluidas++;

    if(threads_centrais_concluidas>=3) //caso as 3 threads centrais tenham sido concluidas, libera a thread 1 (a final)
        sem_post(&sem_fim);

    sem_post(&sem_meio);


    pthread_exit(NULL);
}




//thread 1
void *t1 (void *arg) {
    sem_wait(&sem_fim);   //espera as threads centrais acabarem

    printf("Volte sempre!\n");

    pthread_exit(NULL);
}








//main
int main(int argc, char *argv[]) {

    int i; 
    pthread_t threads[NTHREADS];

    //inicializa os dois semaforos
    sem_init(&sem_meio, 0, 0);
    sem_init(&sem_fim, 0, 0);


    /* Cria as threads */
    pthread_create(&threads[0], NULL, t1, NULL);
    pthread_create(&threads[1], NULL, t2, NULL);
    pthread_create(&threads[2], NULL, t3, NULL);
    pthread_create(&threads[3], NULL, t4, NULL);
    pthread_create(&threads[4], NULL, t5, NULL);



    /* Espera todas as threads completarem */
    for (i = 0; i < NTHREADS; i++) {
        pthread_join(threads[i], NULL);
    }



    /* Desaloca semaforos e termina */
    sem_destroy(&sem_meio);
    sem_destroy(&sem_fim);



    return 0;
}
